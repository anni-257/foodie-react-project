import React from "react"
import ReactDOM from "react-dom/client"
import "./App.css"
import Header from "./components/Header"
import Body from "./components/Body"
import Footer from "./components/Footer"

/***
 * - Header
 *    - Logo image
 *    - Navbar
 * - Body
 *    - Search Bar
 *    - Restraurant List
 *      - Restro Card
 *       - Image
 *       - Name
 *       - Rating
 *       - Cusians
 * - Footer
 *  - Copyright
 *  - info
 */

const App=()=>{
  return(
    <div className="main">
       <Header/>
       <Body/>
       <Footer/>
    </div>
   
  )
}

export default App;