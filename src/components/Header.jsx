import foodlogo from "../assets/foodie_logo.png"

const Title=()=>{
    return(
      <>
        <a href="/">
          <img className="foodlogo" src={foodlogo} alt="Foodie logo" />
        </a>
      </>
    );
  }
  
  const Header=()=>{
    return(
      <div className="header">
        <Title/>
        <div className="nav-bar">
          <ul>
            <li><button>Home</button></li>
            <li><button>About</button></li>
            <li><button>Contact</button></li>
            <li><button>Cart</button></li>
          </ul>
        </div>
      </div>
    )
  }

  export default Header;