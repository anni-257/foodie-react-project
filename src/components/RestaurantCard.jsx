import {IMG_CDN_URL} from "../constants"

const RestraurantCard=({restraurant})=>{
  
    const {name,cloudinaryImageId,avgRating,sla,cuisines,areaName}=restraurant.info;
    return(
      <div className="card">
        <img src={IMG_CDN_URL+cloudinaryImageId} alt="cardImg" />
        <h2>{name}</h2>
        <h3>{avgRating} Stars * {sla?.slaString} mins</h3>
        <p>{cuisines.join(", ")}</p>
        <p>{areaName}</p>
      </div>
    );
}

export default RestraurantCard;
