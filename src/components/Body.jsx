import { restroList } from "../constants";
import RestraurantCard from "./RestaurantCard";

const Body=()=>{
    return(
      <div className="restro-list">
        {
          restroList.map(restaurant =>{
            return <RestraurantCard restraurant={restaurant} key={restaurant.info.id}/>
          })
        }
      </div>
    )
  }

  export default Body;